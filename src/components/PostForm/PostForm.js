import React, {PureComponent} from 'react'
import { connect } from 'react-redux'
import {createPost} from "../../redux/actions";


class PostForm extends PureComponent {

  constructor(props) {
    super(props)

    this.state = {
      title: ''
    }
  }

  handlerSubmitForm = ( event ) => {
    event.preventDefault()
    // Если вы всё же хотите обратиться к полям события асинхронно, вам нужно вызвать event.persist()
    // на событии. Тогда оно будет извлечено из пула, что позволит вашему коду удерживать ссылки на это событие.
    // console.log('Submit form...')
    // console.log('STATE: ', this.state)

    const { title } = this.state

    if(!title.trim()) {
      return
    }

    const newPost = {
      title,
      id: Date.now().toString()
    }
    // console.log('newPost: ', newPost)
    this.props.createPost( newPost )

    this.setState( () => ({
      title: ''
    }))
  }

  handlerChangeInput = ( event ) => {
    event.persist()
    this.setState( (prevState) => {
      return {
        ...prevState,
        ...{ [event.target.name ]: event.target.value }
      }
    })
  }



  render() {

    return (
      <>
        <form onSubmit={this.handlerSubmitForm}>
          <div className="form-group">
            <label htmlFor="title">Post header</label>
            <input
              onChange={ this.handlerChangeInput }
              type="text"
              name={ 'title' }
              className="form-control"
              id="title"
              value={ this.state.title }
            />
          </div>
          <button className='btn btn-success' type={'submit'}>Create post</button>
        </form>
      </>
    )

  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    createPost: (arg) => dispatch( createPost(arg) )
  }
}

export default connect(null, mapDispatchToProps)(PostForm)