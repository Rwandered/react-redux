import React from 'react'
import { connect } from 'react-redux'
import Post from "../Post/Post";

const Posts = ( props ) => {
  console.log('PROPS FROM POSTS.JS: ', props)
  const {syncPost} = props
  if( !syncPost.length) {
    return <p className='text-center'>No posts!</p>
  }
  return syncPost.map( post => <Post post={ post } key={post.id}/>)
}

// mapStateToProps - сопоставить состояние со свойствами
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    syncPost: state.posts.posts
  }
}


export  default connect(mapStateToProps, null)(Posts)